(defsystem     "dryad-clim"
  :version     "0.0.1"
  :serial      t
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :maintainer  "contrapunctus <contrapunctus at disroot dot org>"
  :description "A CLIM code browser to visualize and edit any program as a tree"
  :homepage    "https://codeberg.org/contrapunctus/dryad"
  :bug-tracker "https://codeberg.org/contrapunctus/dryad/issues"
  :depends-on  (:mcclim :anathema :bordeaux-threads :trivia
                :dryad :dryad-cl)
  :components  ((:module "clim"
                 :serial t
                 :components ((:file "package")
                              (:file "display")
                              (:file "frame-commands")))))
