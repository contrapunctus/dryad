(defsystem     "dryad-cl"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :maintainer  "contrapunctus <contrapunctus at disroot dot org>"
  :description "Common Lisp support for the Dryad code browser."
  :homepage    "https://codeberg.org/contrapunctus/dryad"
  :bug-tracker "https://codeberg.org/contrapunctus/dryad/issues"
  :depends-on  (:alexandria :concrete-syntax-tree :eclector :trivia :dryad)
  :serial      t
  :components  ((:module "lang-cl"
                 :serial t
                 :components ((:file "package")
                              (:file "tree")
                              (:file "view")))))
