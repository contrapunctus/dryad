(in-package :dryad)

(defun string-to-keyword (string)
  (intern (string-upcase string) "KEYWORD"))

(defun pathname-extension-keyword (path)
  "Return extension of PATH as a string and as a keyword."
  (let* ((ext     (pathname-type path))
         (keyword (string-to-keyword ext)))
    (values ext keyword)))

(defun extract (start end file)
  "Return contents of FILE between START and END as a string."
  (with-open-file (stream file)
    (file-position stream start)
    (loop for char = (read-char stream nil nil)
          while (and char
                     (<= (file-position stream)
                         end))
          collect char into chars
          finally (return (concatenate 'string chars)))))

(defun indent (string spaces level)
  "Indent STRING with LEVEL * SPACES number of spaces."
  (regex-replace-all
   (create-scanner '(:alternation
                     :start-anchor
                     #\newline #\return
                     (:sequence #\return #\newline)))
   string
   `(:match ,(make-string (* level spaces)
                          :initial-element #\space))))
