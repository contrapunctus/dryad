(in-package :dryad)

(defgeneric load-path-1 (path type directory-p)
  (:documentation "Load source code in PATH.
TYPE is a keyword created from the extension of PATH.
DIRECTORY-P is non-nil for directories, and nil for regular files."))

(defmethod load-path-1
    ((path pathname) (type (eql nil)) (directory-p (eql t)))
  "Load PATH, a directory.")

(defun load-path (path)
  (multiple-value-bind (ext keyword) (pathname-extension-keyword path)
    (load-path-1 path
                 (and ext keyword)
                 (directory-pathname-p path))))

(defclass view ()
  ((name
    :initarg :name :initform nil
    :documentation "A user-facing name for this view")
   (description
    :initarg :description :initform nil
    :documentation "A user-facing description for this view."))
  (:documentation
   "A view is used by `compute-children' to determine the children of a node."))

(defun register-view (view)
  (pushnew view (views *settings*)))

(defun applicable-views (node)
  (loop for view in (views *settings*)
        when (applicable-view-p node view)
          collect view))

(defgeneric applicable-view-p (node view)
  (:documentation
   "Return a list of `compute-children' methods which are applicable
   when called with NODE and VIEW, or nil if there are none."))

(defgeneric compute-children (node view extension &optional depth)
  (:documentation
   "Return a list of `node' instances which are the children of NODE, as determined by VIEW.
If NODE is a file, EXTENSION is the file extension as a keyword."))

(defmethod no-applicable-method ((fun (eql #'dryad:compute-children)) &rest args)
  (declare (ignore fun args)) nil)

(defclass node ()
  ((name :initarg :name
         :initform nil
         :accessor name
         :type (or null string))
   (location :initarg :location
             :initform nil
             :accessor location
             :type (or null pathname)
             :documentation
             "The file or directory associated with this node.")
   (fold-state :initarg :fold-state
               :initform :content
               :accessor fold-state
               :documentation
               "Visibility of this object in a Dryad frontend.
The values supported by default are -
:HIDDEN - do not display anything for this node;
:FOLDED - display only the name of this node;
:OUTLINE - display the name and content of this node, and the names of
its direct successors;
:CONTENT - display the name and content for this node and all of its
successors.")
   (indentation :initarg :indentation
                :initform 2
                :type number
                :documentation
                "Number of spaces to indent this node by, for
                each level of DEPTH.")
   (depth :initarg :depth
          :initform 0
          :accessor depth
          :documentation
          "Depth of this node in a tree, with 0 representing the
          root node.")
   (children :initarg :children
             :initform nil
             :accessor children
             :documentation
             "List of immediate children of this node, as determined by the
    `view' slot and the `compute-children' generic function.")
   (view :initarg :view
         :initform (make-instance 'default-view)
         :accessor view
         :documentation
         "Instance of `view' associated with this node. It determines the value of `children'."))
  (:documentation
   "Abstract base class representing the foldable node of a tree
   displayed by a Dryad frontend. Some examples defined by default are
   `directory', `source-file', and `code-block'."))

(defmethod initialize-instance :after ((node node) &rest initargs)
  (declare (ignore initargs))
  (with-slots (children view depth) node
    (setf children (compute-children node view nil depth))))

(defmethod print-object ((node node) stream)
  (print-unreadable-object (node stream :type t)
    (with-slots (name) node
      (format stream "~a" name))))

(defmethod applicable-view-p ((node node) (view dryad:view))
  (compute-applicable-methods #'dryad:compute-children
                              (list node view nil)))

(defclass directory (node) ())

(defun make-directory (path &key (depth 0) (view (make-instance 'default-view)))
  (make-instance 'directory
                 :location path
                 :name (first (last (pathname-directory path)))
                 :depth depth
                 :view view))

(defclass source-file (node)
  ((extension :initarg :extension
              :initform nil)))

(defun make-source-file (pathname &key (depth 0) (view (make-instance 'default-view)))
  "Return an instance of SOURCE-FILE for PATHNAME, or NIL if PATHNAME is not supported. (i.e.there is no applicable `compute-children' method)"
  (multiple-value-bind (ext keyword) (pathname-extension-keyword pathname)
    (let ((obj (make-instance 'source-file
                              :location pathname
                              :extension ext
                              :depth depth
                              :view view)))
      (when (children obj) obj))))

(defmethod initialize-instance :after ((file source-file) &rest initargs)
  (declare (ignore initargs))
  (with-slots (children view extension depth
               location name) file
    (setf children (compute-children file view (string-to-keyword extension) depth)
          name (format nil "~a.~a"
                       (pathname-name location)
                       (pathname-type location)))))

(defmethod applicable-view-p ((file source-file) (view dryad:view))
  (with-slots (extension) file
    (compute-applicable-methods #'dryad:compute-children
                                (list file view (string-to-keyword extension)))))

(defclass code-block (node concrete-syntax-tree:cst)
  ((language :initarg :language
             :initform nil
             :type (or null dryad:language)
             :documentation "Name of language as a string.")))

#+(or)
(defmethod initialize-instance :after ((code-block code-block) &rest initargs)
  (declare (ignore initargs))
  (setf (name code-block) (code-block-name code-block)))

(defgeneric make-tree-1 (path file-ext directory-p)
  (:documentation "Return a list of files belonging to PROJECT."))

(defmethod make-tree-1
    ((path pathname) (ext (eql nil)) (directory-p (eql t)))
  "Return a source tree from the pathname of a directory."
  (declare (ignore ext directory-p))
  (make-directory path))

(defmethod make-tree-1
    ((path pathname) ext (directory-p (eql nil)))
  "Return a source tree from the pathname of a file."
  (declare (ignore ext directory-p))
  (make-source-file path))

(defun make-tree (pathname)
  "Return an instance of `node' representing PATHNAME, which may be a file or a directory.
Return nil if PATHNAME is a file for which no applicable
`compute-children' method is found."
  (multiple-value-bind (ext keyword) (pathname-extension-keyword pathname)
    (make-tree-1 pathname
                 (and ext keyword)
                 (directory-pathname-p pathname))))
