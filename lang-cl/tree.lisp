(in-package :dryad.cl)

;; Common Lisp support for Dryad

(defclass language (dryad:language) ()
  (:default-initargs :name "lisp"))

(register-language (make-instance 'dryad.cl:language) :lisp)

(defclass code-block (eclector.parse-result:parse-result-client ;; required for Eclector
                      dryad:code-block)
  ((def-type :initarg :def-type
             :initform nil
             :accessor def-type
             :documentation
     "If the source code contained in this code block is a list (i.e.
     not an atom), this slot contains the first element of the list.
     For definitions, this is the definition type.")
   (def-name :initarg :def-name
             :initform nil
             :accessor def-name
             :documentation
     "If the source code contained in this code block is a list (i.e.
     not an atom), this slot contains the second element of the list.
     For definitions, this is the definition name.")
   (lambda-list :initarg :lambda-list
                :initform nil
                :accessor lambda-list
                :type list
                :documentation
                "The lambda list for functions, methods, and macros.")
   (docstring :initarg :docstring
              :initform nil
              :accessor docstring
              :type (or null string))
   (qualifier :initarg :qualifier
              :initform nil
              :accessor qualifier
              :type (or null keyword)
              :documentation
              "The qualifier for methods. NIL for non-methods and primary methods.")
   (package :initarg :package
            :initform nil))
  (:default-initargs :language (get-language :lisp)))

(defmethod print-object ((block code-block) stream)
  (print-unreadable-object (block stream :type t)
    (with-slots (def-type def-name) block
      (when (and def-type def-name)
        (format stream "~A ~A" def-type def-name)))))

(defmethod dryad:code-block-name ((block code-block))
  (with-slots (name def-type def-name qualifier dryad:language %raw) block
    (cond
      ((and def-type def-name (eq def-type 'method))
       (if qualifier
           (format nil "~(~A [~S method]~)" def-name qualifier)
           (format nil "~(~A [primary method]~)" def-name)))
      ((and def-type def-name)
       (format nil "~(~A [~A]~)" def-name def-type))
      ((listp %raw)
       (format nil "~(~A~)" (first %raw)))
      (t (format nil "~(~A~)" (type-of %raw))))))

(defmethod eclector.parse-result:make-expression-result
    ((client code-block) (raw t) (children t) (source t))
  (make-instance 'code-block :raw raw :source source))

(defmethod eclector.make-skipped-input-result
    ((client code-block) (stream t) (reason t) (source t))
  (list :reason reason :source source))

(defmethod parse-definition
    ((block code-block) (first (eql :defpackage)) (rest list))
  (with-slots (def-type def-name) block
    (destructuring-bind (name &rest forms) rest
      (setf def-type 'package
            def-name name)
      block)))

(defmethod parse-definition
    ((block code-block) (first (eql :defvar)) (rest list))
  (destructuring-bind (name &optional value docstring) rest
    (setf (def-type block)  'variable
          (def-name block)  name
          (docstring block) (when (stringp docstring) docstring))
    block))

(defmethod parse-definition
    ((block code-block) (first (eql :defclass)) (rest list))
  (destructuring-bind (name direct-superclasses direct-slots &rest options) rest
    (setf (def-type block)  'class
          (def-name block)  name
          (docstring block) (first (assoc-value options :documentation)))
    block))

(defmethod parse-definition
    ((block code-block) (first (eql :defun)) (rest list))
  (destructuring-bind (name arglist &optional docstring &body body) rest
    (setf (def-type block)    'function
          (def-name block)    name
          (lambda-list block) arglist
          (docstring block)   (when (stringp docstring) docstring))
    block))

(defmethod parse-definition
    ((block code-block) (first (eql :defgeneric)) (rest list))
  (destructuring-bind (name arglist &body options) rest
    (setf (def-type block)    'generic-function
          (def-name block)    name
          (lambda-list block) arglist
          (docstring block)   (first (assoc-value options :documentation)))
    block))

(defmethod parse-definition
    ((block code-block) (first (eql :defmethod)) (rest list))
  (let ((name      (pop rest))
        (qualifier (when (keywordp (first rest))
                     (pop rest)))
        (arglist   (pop rest))
        (docstring (when (stringp (first rest))
                     (pop rest))))
    (setf (def-type block)    'method
          (def-name block)    name
          (qualifier block)   qualifier
          (lambda-list block) arglist
          (docstring block)   docstring)
    block))

(defmethod parse-definition
    ((block code-block) (first (eql :define-condition)) (rest list))
  (setf (def-type block)    'condition
        (def-name block)    (first rest))
  block)

(defmethod parse-definition
    ((block code-block) (first (eql :define-application-frame)) (rest list))
  (destructuring-bind (name superclasses slots &rest options) rest
    (setf (def-type block)    'clim-application-frame
          (def-name block)    name
          (docstring block)   (first (assoc-value options :documentation)))
    block))

(defmethod parse-definition
    ((block code-block) (first (eql :define-presentation-to-command-translator)) (rest list))
  (destructuring-bind (name options arglist &body body) rest
    (let ((first-body (first body)))
      (setf (def-type block)    'presentation-command-translator
            (def-name block)    name
            (docstring block)   (when (stringp first-body) first-body))
      block)))

(defmethod dryad:compute-children
    ((file dryad:source-file) (view dryad:default-view) (ext (eql :lisp))
     &optional (depth 0))
  (with-accessors ((location dryad:location)) file
    (let ((form-depth (1+ depth)))
      (with-open-file (stream location :external-format :utf-8)
        (loop
          with current-package = (find-package :cl)
          for block = (ignore-errors
                       (eclector.parse-result:read (make-instance 'code-block)
                                                   stream nil nil))
          while block
          do (with-slots (depth %raw def-type def-name name package) block
               (setf depth   form-depth
                     ;; Set PACKAGE slot /before/ it is changed by an
                     ;; IN-PACKAGE, so the value of the package slot for
                     ;; IN-PACKAGE forms is the CL package
                     package current-package))
             (match (raw block)
               ((list* first second _)
                (let* ((rest          (rest (raw block)))
                       (first-keyword (intern (symbol-name first) "KEYWORD"))
                       (args          (list block first-keyword rest)))
                  (when (compute-applicable-methods #'parse-definition args)
                    (setf block (apply #'parse-definition args))))
                ;; Encountered an IN-PACKAGE form - update future values
                ;; of CURRENT-PACKAGE
                (when (eq 'in-package first)
                  (setf current-package (find-package second)))))
             (setf (name block) (dryad:code-block-name block))
          collect block)))))

(defmethod dryad:load-path-1
    ((path pathname) (type (eql :asd)) (directory-p (eql nil)))
  "Load project designated by PATH, an ASDF file."
  (asdf:load-asd path))

(defmethod dryad:load-path-1
    ((path pathname) (type (eql :lisp)) (directory-p (eql nil)))
  "Load project designated by PATH, a Lisp source file."
  (load path))
