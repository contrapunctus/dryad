(in-package :dryad.cl)

(defclass asdf-view (dryad:view) ()
  (:default-initargs
   :name "ASDF"
   :description "Display systems from an ASDF system definition file, or any such files found in a directory. The components of each system are displayed as its children."))

(register-view (make-instance 'asdf-view))

(defmethod dryad:compute-children
    ((file dryad:source-file) (view asdf-view) (ext (eql :asd)) &optional (depth 0))
  (with-accessors ((path dryad:location)) file
    (asdf:load-asd path)
    (asdf:component-children
     (asdf:find-system
      (asdf:primary-system-name
       (pathname-name path))))))

(defmethod dryad:compute-children
    ((directory dryad:directory) (view asdf-view) ext &optional (depth 0))
  (with-accessors ((location dryad:location)
                   (depth dryad:depth)) directory
    (let (files)
      (walk-directory
       location
       (lambda (path)
         (when (equal (pathname-type path) "asd")
           (pushnew (make-source-file path :depth depth :view view)
                    files)))
       :directories t)
      files)))
