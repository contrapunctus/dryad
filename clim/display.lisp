(in-package :dryad.clim)

(defun display-pane (frame stream)
  (with-output-recording-options (stream :record t :draw nil)
    (with-slots (trees) frame
      (if trees
          (loop for tree in trees
                do (display tree (fold-state tree) stream))
          (format stream "Nothing to display. Add something!"))))
  (stream-replay stream +everywhere+))

(defgeneric display (object fold-state stream &optional arg)
  (:documentation "Print OBJECT to STREAM."))

;; Methods to display node headings. I probably wrote them this way to
;; make it easy for users to replace them for individual node types.
(defun display-heading-default (object class fold-state stream)
  (unless (eq fold-state :hidden)
    (with-slots (name indentation depth) object
      (with-output-as-presentation (stream object class)
        (with-drawing-options (stream :ink (elt (heading-colors *application-frame*) depth))
          (format stream "~%~A" (indent name indentation depth))
          (match fold-state
            ((or :folded :outline)
             (format stream "..."))))))))

(defmethod display :before
    ((file dryad:source-file) fold-state stream &optional arg)
  (declare (ignore arg))
  (display-heading-default file 'dryad:source-file fold-state stream))

(defmethod display :before
    ((dir dryad:directory) fold-state stream &optional arg)
  (declare (ignore arg))
  (display-heading-default dir 'dryad:directory fold-state stream))

(defmethod display :before
    ((code-block dryad:code-block) fold-state stream &optional arg)
  (declare (ignore arg))
  (display-heading-default code-block 'dryad:code-block fold-state stream))

(defmethod display :after
    ((block dryad:code-block) fold-state stream &optional arg)
  "Display a newline after the contents of BLOCK, when its FOLD-STATE
is not :HIDDEN, :OUTLINE, or :FOLDED."
  (declare (ignore node arg))
  (match fold-state
    ((not (or :hidden :outline :folded))
     (format stream "~%"))))

(defmethod display
    ((node dryad:node) fold-state stream &optional arg)
  (declare (ignore node fold-state stream arg)))

(defun filter-by-type (type list)
  (remove-if-not (lambda (obj)
                   (typep obj type))
                 list))

(defmethod display
    ((dir dryad:directory) (fold-state t) stream &optional arg)
  (declare (ignore arg))
  (match fold-state
    ((or :outline :content)
     (with-output-as-presentation (stream dir 'dryad:directory)
       (with-slots (children) dir
         (let ((files   (filter-by-type 'source-file children))
               (subdirs (filter-by-type 'dryad:directory children)))
           (loop for file in files
                 do (with-slots (fold-state) file
                      (display file fold-state stream)))
           (loop for subdir in subdirs
                 do (with-slots (fold-state) subdir
                      (display subdir fold-state stream)))))))))

(defmethod display
    ((file source-file) fold-state stream &optional arg)
  "Display FILE and the definitions within it."
  (declare (ignore arg))
  (match fold-state
    ((or :outline :content)
     (with-slots (location children name fold-state indentation depth) file
       (with-output-as-presentation (stream file 'source-file)
         (loop for form in children
               do (with-slots (fold-state) form
                    (display form fold-state stream (location file)))))))))

(defmethod display
    ((code-block code-block) (fold-state (eql :outline)) stream &optional file)
  "No-op to handle display of OUTLINE'd code-blocks."
  (declare (ignore code-block fold-state stream file)))

(defmethod display
    ((code-block code-block) (fold-state (eql :content)) stream &optional file)
  "Display text from FILE using the source locations provided by CODE-BLOCK.
Text is extracted from FILE to retain and display source comments within the
code."
  (with-slots (name indentation depth fold-state) code-block
    (let* ((source    (source code-block))
           (substring (extract (first source) (rest source) file)))
      (with-drawing-options (stream :text-style (code-block-text-style *application-frame*))
        (with-output-as-presentation (stream code-block 'code-block)
          (format stream "~%~a" (indent substring indentation depth)))))))
