(in-package :dryad.clim)

(define-application-frame dryad ()
  ((trees :initarg :trees
          :initform nil
          :accessor trees
          :documentation
          "List of `node' instances currently displayed by the UI.")
   (heading-size-by-depth
    :initarg :heading-size-by-depth
    :initform t
    :accessor heading-size-by-depth
    :documentation
    "Non-nil means enable scaling of heading sizes based on their depth.")
   (heading-colors :initarg :heading-colors
                   :initform (a:heading-foreground-inks)
                   :accessor heading-colors
                   :documentation
                   "A circular list of colors to use for headings.")
   (code-block-text-style :initarg :code-block-text-style
                          :accessor code-block-text-style
                          :initform (make-text-style :fix :roman nil)
                          :documentation
                          "Text style for display of source code blocks."))
  (:menu-bar t)
  (:pointer-documentation t)
  (:panes (dryad :application
                 :height (graft-height (find-graft))
                 :width (graft-width (find-graft))
                 :display-function 'display-pane
                 :incremental-redisplay t
                 :background (a:style-background-ink as:*default*)
                 :foreground (a:style-foreground-ink as:*default*))
          (int :interactor
               :width (/ (graft-width (find-graft)) 2)
               :background (a:style-background-ink as:*default*)
               :foreground (a:style-foreground-ink as:*default*)))
  (:layouts (default (horizontally () dryad int))))

(defun update-child-fold-states (node new-state)
  "Update fold state of NODE's children to NEW-STATE, recursively."
  (let ((children (children node)))
    (when children
      (loop for obj in children
            do (setf (fold-state obj) new-state)
               (update-child-fold-states obj new-state)))
    node))

(defgeneric cycle-fold-state (node)
  (:documentation "Return NODE with fold-state updated."))

(defmethod cycle-fold-state ((node node))
  "Cycle fold state for directories and files between :FOLDED,
:OUTLINE, and :CONTENT."
  (case (fold-state node)
    (:folded
     (setf (fold-state node) :outline)
     (update-child-fold-states node :folded))
    (:outline
     (setf (fold-state node) :content)
     (update-child-fold-states node :content))
    (:content
     (setf (fold-state node) :folded)
     (update-child-fold-states node :hidden))))

(defmethod cycle-fold-state ((code-block code-block))
  "Switch fold-state of CODE-BLOCK between :FOLDED and :CONTENT."
  (setf (fold-state code-block)
        (if (eq (fold-state code-block) :folded)
            :content
            :folded)))

(define-dryad-command (com-refresh :name t) ()
  (with-slots (trees) *application-frame*
    (setf trees
          (loop for tree in trees
                collect (make-tree (location tree))))))

(define-dryad-command (com-insert-tree :name t :menu t) ((pathname 'pathname))
  (load-path pathname)
  (pushnew (make-tree pathname) (trees *application-frame*)))

(define-dryad-command (com-remove-tree :name t :menu t) ((node 'node))
  (with-slots (trees) *application-frame*
    (setf trees (remove node trees :test #'equal))))

(define-dryad-command (com-cycle :name t :menu t) ((node 'node))
  (cycle-fold-state node)
  (format (find-pane-named *application-frame* 'int)
          "node: ~S ~S" node (fold-state node)))

(define-presentation-to-command-translator com-cycle
    (node com-cycle dryad)
    (node)
  (list node))

(defun run (&rest pathnames)
  (let ((trees (loop for path in pathnames
                     do (load-path path)
                     collect (make-tree path))))
    (bt:make-thread
     (lambda ()
       (run-frame-top-level
        (make-application-frame 'dryad :trees trees))))))

;; 1. try to load project
;;    * accept either a directory or a system definition
;; 2. read files
;; 3. display trees
